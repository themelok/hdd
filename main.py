import sys
from abc import ABCMeta, abstractmethod
import subprocess
import re


class Abstract:
    __metaclass__ = ABCMeta

    @abstractmethod
    def get_blocks(self):
        raise NotImplementedError

    @abstractmethod
    def get_devices(self):
        raise NotImplementedError

    @abstractmethod
    def get_partitions(self, devise):
        raise NotImplementedError


class ObtainLinuxBlockDevices(Abstract):
    def __init__(self):
        self.blocks = self.get_blocks()

    def get_blocks(self):
        """Generates dicts of all block devices and partitions""" 
        regex = re.compile('([A-Z]+)=(?:"(.*?)")')
        blocks = []
        output = subprocess.check_output(["lsblk", "-P", "-o", "NAME,SIZE,TYPE"])
        for l in output.strip().splitlines():
            blocks.append(dict([(k.lower(), v) for k, v in regex.findall(l)]))
        return blocks

    def get_devices(self):
        """Generates list of dicts of all block devices"""
        disks = [d for d in self.blocks if d['type'] == 'disk']
        return disks

    def get_partitions(self, devise_no):
        """Generates a list of partitions of the selected device"""
        try:
            disks = self.get_devices()
            disk = disks[int(devise_no) - 1]
            parts = dict()
            parts[disk['name']] = [part for part in self.blocks if
                                   disk['name'] in part['name'] and part['type'] == 'part']
            return parts
        except:
            print 'Wrong device number.'

    def output_partitions(self, devise_no):
        parts = self.get_partitions(devise_no)
        if parts:
            for k, v in parts.items():
                print 'Partitions of ', k
                print '%5s %10s %10s' % ('No', 'Partition', 'Size')
                for i in enumerate(v):
                    print '%5s %10s %10s' % (i[0] + 1, i[1]['name'], i[1]['size'])

    def output_devices(self):
        disks = self.get_devices()
        print '%5s %10s %10s' % ('No', 'Devices', 'Size')
        for disk in enumerate(disks):
            print '%5s %10s %10s' % (disk[0] + 1, disk[1]['name'], disk[1]['size'])

        print "\nYou can obtain partitions of each devices by specifying device number as command line argument."


class ObtainWindowsBlockDevice(Abstract):
    def __init__(self):
        self.blocks = self.get_blocks()

    @staticmethod
    def sizeof_human_readable(num, suffix='B'):
        for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
            if abs(num) < 1024.0:
                return "%3.1f%s%s" % (num, unit, suffix)
            num /= 1024.0
        return "%.1f%s%s" % (num, 'Yi', suffix)

    def get_blocks(self):
        """Generates dicts of all block devices or all partitions""" 
        if len(sys.argv) > 1:
            blocks = []
            regex = re.compile('([a-zA-Z]+)=(\w*\s?#?\d,?\s?\w+\s?#?\d?)')
            output = subprocess.check_output(['wmic', 'partition', 'get', 'Name,', 'Size', '/format:list'])
            s = output.replace('\r\r\n\r\r\n\r\r\n', '|').replace('\r\r\n', ' ')
            for l in s.strip().split('|'):
                blocks.append(dict([(k.lower(), v) for k, v in regex.findall(l)]))
        else:
            regex = re.compile('(PHYSICALDRIVE\d+\s*\d*)')
            blocks = []
            output = subprocess.check_output(['wmic', 'diskdrive', 'get', 'name,', 'size'])
            for l in output.strip().splitlines():
                for v in regex.findall(l):
                    if v:
                        blocks.append(v.split())
        return blocks

    def get_devices(self):
        """Generates list of dicts of all block devices"""
        drives = [{'name': d[0], 'size': self.sizeof_human_readable(int(d[1]))} for d in self.blocks]
        return drives

    def get_partitions(self, devise_no):
        """Generates a list of partitions of the selected device"""
        try:
            parts = dict()
            devise_no = int(devise_no) - 1
            parts['PHYSICALDRIVE%s' % devise_no] = [part for part in self.blocks if
                                                    part and 'Disk #%s' % devise_no in part['name']]
            if parts['PHYSICALDRIVE%s' % devise_no]:
                return parts
            else:
                # TODO: This is worst part, but it's 01:30 AM now :(
                print 'Wrong device number.'
        except:
            print 'Wrong device number.'

    def output_devices(self):
        disks = self.get_devices()
        print '%5s %15s %10s' % ('No', 'Devices', 'Size')
        for disk in enumerate(disks):
            print '%5s %15s %10s' % (disk[0] + 1, disk[1]['name'], disk[1]['size'])

        print "\nYou can obtain partitions of each devices by specifying device number as command line argument."

    def output_partitions(self, devise_no):
        parts = self.get_partitions(devise_no)
        if parts:
            for k, v in parts.items():
                print 'Partitions of ', k
                print '%5s %25s %10s' % ('No', 'Partition', 'Size')
                for i in enumerate(v):
                    print '%5s %25s %10s' % (i[0] + 1, i[1]['name'], self.sizeof_human_readable(int(i[1]['size'])))


if __name__ == '__main__':
    if sys.platform == 'linux2':
        dev = ObtainLinuxBlockDevices()
        if len(sys.argv) > 1:
            dev.output_partitions(sys.argv[1])
        else:
            dev.output_devices()
    elif sys.platform == 'win32':
        dev = ObtainWindowsBlockDevice()
        if len(sys.argv) > 1:
            dev.output_partitions(sys.argv[1])
        else:
            dev.output_devices()
