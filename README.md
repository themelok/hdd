# Outputs #

Samples of outputs

### Ubuntu outputs ###

```
#!bash
sysad@io:~/Downloads$ python main.py 
   No    Devices       Size
    1    nvme0n1     238,5G

You can obtain partitions of each devices by specifying device number as command line argument.
```

```
#!bash
sysad@io:~/Downloads$ python main.py 1
Partitions of  nvme0n1
   No  Partition       Size
    1  nvme0n1p1       450M
    2  nvme0n1p2       100M
    3  nvme0n1p3        16M
    4  nvme0n1p4     116,7G
    5  nvme0n1p5       3,7G
    6  nvme0n1p6     117,6G

```
### CentOS7 outputs ###

```
#!bash

[sysad@localhost ~]$ python main.py 
   No    Devices       Size
    1        sda         8G
    2        sdb        20G

You can obtain partitions of each devices by specifying device number as command line argument.

```

```
#!bash

[sysad@localhost ~]$ python main.py 1
Partitions of  sda
   No  Partition       Size
    1       sda1         1G
    2       sda2       2,8G
[sysad@localhost ~]$ python main.py 2
Partitions of  sdb
   No  Partition       Size
    1       sdb1        20G

```


### Windows outputs ###


```
#!bash

C:\Users\User\hdd>python main.py
   No         Devices            Size
    1  PHYSICALDRIVE0          25.0GB
    2  PHYSICALDRIVE1          10.2GB

You can obtain partitions of each devices by specifying device number as command line argument.
```


```
#!bash
C:\Users\User\hdd>python main.py 1
Partitions of  PHYSICALDRIVE0
   No                 Partition       Size
    1     Disk #0, Partition #0    350.0MB
    2     Disk #0, Partition #1     24.7GB

```